defmodule ExKevaTest do
  use ExUnit.Case

  @moduletag :capture_log

  setup do

    Application.stop(:exkeva)
    :ok = Application.start(:exkeva)
  end

  test "initial state" do
    assert ExKeva.which_buckets() == []
  end

  test "create bucket" do
    assert ExKeva.create(:test_bucket) == :ok

    assert ExKeva.which_buckets() == [:test_bucket]
  end

  test "delete bucket" do
    ExKeva.create(:test_bucket)

    assert ExKeva.delete(:test_bucket) == :ok

    assert ExKeva.which_buckets() == []
  end

  test "add key value" do
    :ok = ExKeva.create(:test_bucket)

    assert ExKeva.put(:test_bucket, "key", "value") == :ok

    assert ExKeva.get(:test_bucket, "key") == {:ok, "value"}
  end

  test "drop key" do
    :ok = ExKeva.create(:test_bucket)

    assert ExKeva.put(:test_bucket, "key", "value") == :ok

    assert ExKeva.get(:test_bucket, "key") == {:ok, "value"}

    assert ExKeva.drop(:test_bucket, "key") == :ok

    assert ExKeva.get(:test_bucket, "key") == {:ok, {:error, :no_key_found}}

  end
end
