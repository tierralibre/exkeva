defmodule ExKeva do
  @moduledoc """
  The ExKeva interface.

  > This module implements buckets and keys.
  > A bucket contains keys and each key is linked to a process which takes
  > care of is own ttl.
  > Keys can be located on different nodes which are part of the same bucket.
  >
  > If a key terminates, it is automatically removed from the bucket.
  """

  alias ExKeva.{Impl, Worker}

  @doc """
  Create a bucket with given `name`.

  > Creates a new, empty bucket. The bucket is globally visible on all
  > nodes. If the bucket exists, nothing happens.
  """

  def create(name) do
    unless Impl.bucket_exists?(name) do
      Worker.globally_locked_multi_call(name, {:create, name})
    end

    :ok
  end

  @doc """
  Delete the bucket with given `name`.
  """

  def delete(name) do
    Worker.globally_locked_multi_call(name, {:delete, name})

    :ok
  end


  @doc """
  Put a key and value on the bucket .
  """

  def put(bucket, key, value) do
    if Impl.bucket_exists?(bucket) do
      Worker.globally_locked_multi_call(bucket, {:put, bucket, key, value})
      :ok
    else
      {:error, {:no_such_bucket, bucket}}
    end
  end

  def put(bucket, key, value, ttl) do
    if Impl.bucket_exists?(bucket) do
      Worker.globally_locked_multi_call(bucket, {:put, bucket, key, value, ttl})
      :ok
    else
      {:error, {:no_such_bucket, bucket}}
    end
  end



  @doc """
  Get the value from the bucket with key.
  """

  def get(bucket, key) do
    if Impl.bucket_exists?(bucket) do
     {[{_, value}], _} = Worker.globally_locked_multi_call(bucket, {:get, bucket, key})
     #value = Worker.globally_locked_multi_call(bucket, {:get, bucket, key})
     IO.inspect value
      {:ok, value}
    else
      {:error, {:no_such_bucket, bucket}}
    end
  end

  @doc """
  Drop a key from the bucket.
  """
  def drop(bucket, key) do
    if Impl.bucket_exists?(bucket) do
      {[{_, response}], _} = Worker.globally_locked_multi_call(bucket, {:drop, bucket, key})
      response
    else
      {:error, {:no_such_bucket, bucket}}
    end
  end

  @doc """
  Get all keys from the process bucket with given `name`.
  """

  def get_bucket_pids(bucket) do
    if Impl.bucket_exists?(bucket) do
      Impl.get_bucket_pids(bucket)
    else
      {:error, {:no_such_bucket, bucket}}
    end
  end

  @doc """
  Get local pids for a bucket.
  """

  def get_local_pids(bucket) do
    if Impl.bucket_exists?(bucket) do
      Impl.local_bucket_pids(bucket)
    else
      {:error, {:no_such_bucket, bucket}}
    end
  end

  @doc """
  Get a list of all known buckets.
  """
  def which_buckets(), do: Impl.all_buckets()
end
