defmodule ExKeva.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      worker(ExKeva.Worker, []),
    ]

    opts = [strategy: :one_for_one, name: ExKeva.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
