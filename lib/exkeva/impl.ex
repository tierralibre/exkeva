defmodule ExKeva.Impl do
  @moduledoc false

  alias ExKeva.ETS

  def init(), do: ETS.new()

  def assure_bucket(name) do
    key = {:bucket, name}

    unless ETS.member(key), do: ETS.insert({key})
  end

  def bucket_exists?(name), do: ETS.member({:bucket, name})

  def put(bucket, key, pid) when is_pid(pid) do
    # can be extracted not to repeate code by getting information from Value
    # GenServer not don't as is a PoC
    ref_pid_key = {:ref, pid}

    ETS.update_counter ref_pid_key, {4, +1}, on_failure: fn _, _ ->
      monitor_ref = Process.monitor(pid)

      ETS.insert({ref_pid_key, monitor_ref, 1})
    end

    key_bucket_pid = {:key, bucket, pid, key}

    ETS.update_counter key_bucket_pid, {2, +1}, on_failure: fn _, _ ->
      ETS.insert({key_bucket_pid, 1})

      if node(pid) == Node.self(), do: ETS.insert({{:local_key, bucket, pid, key}})

      ETS.insert({{:pid, pid, bucket, key}})
    end

    :ok
  end

  # to do handle only one key
  def put(bucket, key, value) do
    {:ok, pid} = ExKeva.Value.start_link([bucket: bucket, key: key, value: value])
    do_put(bucket, key, pid)
  end

  def put(bucket, key, value, ttl) do
    {:ok, pid} = ExKeva.Value.start_link([bucket: bucket, key: key, value: value, ttl: ttl])
    do_put(bucket, key, pid)
  end

  defp do_put(bucket, key, pid) when is_pid(pid) do
    ref_pid_key = {:ref, pid}

    ETS.update_counter ref_pid_key, {4, +1}, on_failure: fn _, _ ->
      monitor_ref = Process.monitor(pid)

      ETS.insert({ref_pid_key, monitor_ref, 1})
    end

    key_bucket_pid = {:key, bucket, pid, key}

    ETS.update_counter key_bucket_pid, {2, +1}, on_failure: fn _, _ ->
      ETS.insert({key_bucket_pid, 1})

      if node(pid) == Node.self(), do: ETS.insert({{:local_key, bucket, pid, key}})

      ETS.insert({{:pid, pid, bucket, key}})
    end

    :ok
  end

  def drop(bucket, pid) when is_pid(pid) do
    case ETS.match({{:key, bucket, pid, :"$1"}, :"$2"}) do
      [] ->
        {:error, :no_key_found}
      [[key, _bucket_counter]] ->
        do_drop(bucket, key, pid)
    end
  end

  def drop(bucket, key) do
    case ETS.match({{:key, bucket, :"$1", key}, :"$3"}) do
      [] ->
        {:error, :no_key_found}
      [[pid, _bucket_counter]] ->
        do_drop(bucket, key, pid)
    end
  end

  defp do_drop(bucket, key, pid) do
    key_bucket_pid = {:key, bucket, pid, key}
    ETS.update_counter key_bucket_pid, {2, -1},
      on_success: fn bucket_counter ->
        if bucket_counter == 0 do
          ETS.delete({:pid, pid, bucket, key})

          if node(pid) == Node.self(),
            do: ETS.delete({:local_key, bucket, pid, key})

          ETS.delete(key_bucket_pid)
        end

        ref_pid_key = {:ref, pid}

        if ETS.update_counter(ref_pid_key, {3, -1}) == 0 do
          [{^ref_pid_key, monitor_ref, 0}] = ETS.lookup(ref_pid_key)

          ETS.delete(ref_pid_key)

          true = Process.demonitor(monitor_ref, [:flush])
        end
      end

  end

  def delete_bucket(name) do
    for pid <- bucket_pids(name), do: drop(name, pid)

    ETS.delete({:bucket, name})
  end

  def bucket_pids(name) do
    for [pid, key, bucket_counter] <- ETS.match({{:key, name, :"$1", :"$2"}, :"$3"}),
        _ <- 1..bucket_counter,
      do: pid
  end

  def bucket_pids_and_keys(name) do
    for [pid, key, bucket_counter] <- ETS.match({{:key, name, :"$1", :"$2"}, :"$3"}),
        _ <- 1..bucket_counter,
      do: {pid, key}
  end

  def bucket_pids_and_pids(bucket) do
    for [pid, key, bucket_counter] <- ETS.match({{:key, bucket, :"$1", :"$2"}, :"$3"}),
        _ <- 1..bucket_counter
    do
      {pid, key}
    end
  end

  def get_bucket_pids(bucket) do
    for [pid, key, bucket_counter] <- ETS.match({{:key, bucket, :"$1", :"$2"}, :"$3"}),
        _ <- 1..bucket_counter,
      do: pid
  end

  def get(bucket, key) do
    for [pid, bucket_counter] <- ETS.match({{:key, bucket, :"$1", key}, :"$2"}),
        _ <- 1..bucket_counter,
      do: pid
  end

  def local_bucket_pids(name) do
    for [pid, key] <- ETS.match({{:local_key, name, :"$1", :"$2"}}),
        key <- keys_in_bucket(pid, name, key),
      do: key
  end

  def local_bucket_pids_and_keys(name) do
    for [pid, key] <- ETS.match({{:local_key, name, :"$1", :"$2"}}),
        key <- keys_in_bucket(pid, name, key),
      do: {pid, key}
  end

  def all_buckets(), do: for [name] <- ETS.match({{:bucket, :"$1"}}), do: name

  def bucket_keys(pid) do
    for [name, key] <- ETS.match({{:pid, pid, :"$1", :"$2"}}), do: [name, key]
  end

  def keys_in_bucket(pid, name, key) do
    case ETS.lookup({:key, name, pid, key}) do
      [] ->
        []

      [{{:key, ^name, ^pid, ^key}, bucket_counter}] ->
        List.duplicate(pid, bucket_counter)
    end
  end

  def keys_and_pid_in_bucket(pid, name, key) do
    case ETS.lookup({:key, name, pid, key}) do
      [] ->
        []

      [{{:key, ^name, ^pid, ^key}, bucket_counter}] ->
        List.duplicate(key, bucket_counter)
    end
  end
end
