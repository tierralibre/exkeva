defmodule ExKeva.Worker do
  @moduledoc false

  use GenServer

  require Logger

  alias ExKeva.Impl

  def start_link(), do: GenServer.start_link(__MODULE__, [], name: __MODULE__)

  @doc """
  Make a globally locked multi call to all `ExKeva.Worker`s in the cluster.
  """
  def globally_locked_multi_call(bucket, message) do
    :global.trans {{__MODULE__, bucket}, self()}, fn ->
      all_nodes = Node.list([:visible, :this])

      GenServer.multi_call(all_nodes, ExKeva.Worker, message)
    end
  end


  def init([]) do
    nodes = Node.list()

    :ok = :net_kernel.monitor_nodes(true)

    for new_node <- nodes do
      send worker_for(new_node), {:new_exkeva, Node.self()}
      send self(), {:nodeup, new_node}
    end

    :ok = Impl.init()

    {:ok, %{}}
  end


  def handle_call({:create, bucket}, _from, state) do
    Impl.assure_bucket(bucket)

    {:reply, :ok, state}
  end

  def handle_call({:put, bucket, key, value}, _from, state) do
    if Impl.bucket_exists?(bucket), do: Impl.put(bucket, key, value)

    {:reply, :ok, state}
  end

  def handle_call({:put, bucket, key, value, ttl}, _from, state) do
    if Impl.bucket_exists?(bucket), do: Impl.put(bucket, key, value, ttl)

    {:reply, :ok, state}
  end

  def handle_call({:drop, bucket, key}, _from, state) do
    if Impl.bucket_exists?(bucket), do: Impl.drop(bucket, key)

    {:reply, :ok, state}
  end

  def handle_call({:get, bucket, key}, _from, state) do
    pids =  if Impl.bucket_exists?(bucket), do: Impl.get(bucket, key)

    {:reply, get_value(pids), state}
  end

  defp get_value(pids) when length(pids) == 0 do
    {:error, :no_key_found}
  end

  defp get_value(pids) when length(pids) == 1 do
    pids
    |> List.first()
    |> ExKeva.Value.value()
  end

  defp get_value(pids) when length(pids) > 1 do
    Enum.map(pids, fn(pid) ->
      ExKeva.Value.value(pid)
    end)
  end

  def handle_call({:delete, bucket}, _from, state) do
    Impl.delete_bucket(bucket)

    {:reply, :ok, state}
  end

  def handle_call(message, from, state) do
    _ = Logger.warn(
      """
      unexpected message:
      handle_call(#{inspect message}, #{inspect from}, #{inspect state})
      """
    )

    {:noreply, state}
  end


  def handle_cast({:exchange, _node, all_keys}, state) do
    for {bucket, pids_and_keys} <- all_keys,
        Impl.assure_bucket(bucket),
        {pid, key} <- pids_and_keys -- Impl.local_bucket_pids_and_keys(bucket)
    do
      Impl.put(bucket, key, pid)
    end

    {:noreply, state}
  end

  def handle_cast(_, state), do: {:noreply, state}


  def handle_info({:DOWN, _ref, :process, pid, _info}, state) do
    IO.puts "pid is DOWN ... #{inspect(pid)}"
    # test me
    for [bucket, key] <- Impl.bucket_keys(pid),
        pid <- Impl.keys_in_bucket(pid, bucket, key)
    do
      Impl.drop(bucket, key)
    end

    {:noreply, state}
  end

  def handle_info({:nodeup, new_node}, state) do
    exchange_all_keys(new_node)

    {:noreply, state}
  end

  def handle_info({:new_exkeva, new_node}, state) do
    exchange_all_keys(new_node)

    {:noreply, state}
  end

  def handle_info(_, state), do: {:noreply, state}

  defp exchange_all_keys(node_name) do
    all_keys =
      for bucket <- Impl.all_buckets(), do: {bucket, Impl.bucket_pids_and_keys(bucket)}

    node_name
    |> worker_for()
    |> GenServer.cast({:exchange, Node.self(), all_keys})
  end

  defp worker_for(node_name), do: {__MODULE__, node_name}
end
