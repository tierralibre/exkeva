defmodule ExKeva.Value do
  use GenServer
  require Logger

  @default_opts [name: __MODULE__]
  @default_ttl Application.get_env(:exkeva, :ttl) || :timer.hours(24)

  defmodule State do
    defstruct ttl: nil, timer_ref: nil, bucket: nil, key: nil, value: nil
  end

  def value(pid) do
    GenServer.call(pid, :value)
  end

  def start_link(opts \\ @default_opts) do
    #keeping the info cached for simplicity we could do it in ets
    bucket = Keyword.get(opts, :bucket)
    key = Keyword.get(opts, :key)
    value = Keyword.get(opts, :value)
    ttl = Keyword.get(opts, :ttl) || @default_ttl
    GenServer.start_link(__MODULE__, %State{bucket: bucket, key: key, value: value, ttl: ttl}, [])
  end

  def init(state) do
    Logger.info "Value init with state: #{inspect(state)}"

    Process.send_after(self(), :schedule, 0)

    {:ok, state}
  end

  def handle_call(:value, _from, state) do
    {:reply, state.value, state}
  end

  def handle_info(:exit, state) do

    ExKeva.drop(state.bucket, self())
    {:noreply, state}
  end

  def handle_info(:schedule, state) do
    do_schedule(state)
    {:noreply, state}
  end

  defp do_schedule(state) do
    if state.timer_ref do
      Process.cancel_timer(state.timer_ref)
    end

    Logger.debug "ttl schedule: #{inspect(state.ttl)}"

    timer_ref = Process.send_after(self(), :exit, state.ttl)

    %State{state | timer_ref: timer_ref}
  end

end
